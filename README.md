# Edu

elixir 1.6, erlang 20, mysql 5.6

To start:
 
  * Configure database (if necessary) `config/dev.exs`
  * Install dependencies with `mix deps.get`
  * Script for populating the database `mix run priv/repo/seeds.exs`
  * Start Phoenix endpoint with `mix phx.server`

