defmodule Edu do
  @moduledoc """
  Edu keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """
  import Ecto.Query, only: [from: 2]

  alias Edu.Exporter
  alias Edu.Source

  def export_source do
    query =
      from(
        s in Source,
        select: [s.a, s.b, s.c]
      )

    Exporter.export(query)
  end
end
