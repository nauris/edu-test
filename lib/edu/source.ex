defmodule Edu.Source do
  use Ecto.Schema

  @primary_key false
  schema "source" do
    field(:a, :integer)
    field(:b, :integer)
    field(:c, :integer)
  end
end
