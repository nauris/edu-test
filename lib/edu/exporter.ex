defmodule Edu.Exporter do
  def export(query) do
    query
    |> Edu.Repo.stream()
    |> CSV.encode()
  end
end
