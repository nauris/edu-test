defmodule EduWeb.SourceController do
  use EduWeb, :controller

  def index(conn, _params) do
    conn =
      conn
      |> put_resp_header("content-disposition", "attachment; filename=source.csv")
      |> put_resp_content_type("text/csv")
      |> send_chunked(200)

    {:ok, conn} =
      Edu.Repo.transaction(
        fn ->
          Edu.export_source()
          |> Enum.reduce_while(conn, fn data, conn ->
            case chunk(conn, data) do
              {:ok, conn} ->
                {:cont, conn}

              {:error, :closed} ->
                {:halt, conn}
            end
          end)
        end,
        timeout: :infinity
      )

    conn
  end
end
