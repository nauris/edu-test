# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Edu.Repo.insert!(%Edu.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

# Source table data
1..1_000_000
|> Stream.map(fn x -> [a: x, b: rem(x, 3), c: rem(x, 5)] end)
|> Stream.chunk_every(20_000)
|> Enum.each(fn x ->
  Edu.Repo.insert_all(Edu.Source, x)
end)
