use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :edu, EduWeb.Endpoint,
  secret_key_base: "l5l7e5tF5TpU4rAIMv10FiV0F7hJA66bFhohUwUwSMjv15hJHFloxRsnKKlL+IGj"

# Configure your database
config :edu, Edu.Repo,
  adapter: Ecto.Adapters.MySQL,
  username: System.get_env("DATA_DB_USER"),
  password: System.get_env("DATA_DB_PASS"),
  database: "foo",
  pool_size: 15
